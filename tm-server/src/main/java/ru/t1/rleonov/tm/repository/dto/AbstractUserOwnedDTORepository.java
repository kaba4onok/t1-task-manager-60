package ru.t1.rleonov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.rleonov.tm.enumerated.Sort;

@NoArgsConstructor
public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M>
        implements IUserOwnedDTORepository<M> {

    @NotNull
    protected String getSortType(@Nullable final Sort sort) {
        if (Sort.BY_CREATED == sort) return "created";
        if (Sort.BY_STATUS == sort) return "status";
        else return "name";
    }

}
