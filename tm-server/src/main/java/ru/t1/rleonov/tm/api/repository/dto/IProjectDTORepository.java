package ru.t1.rleonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import ru.t1.rleonov.tm.enumerated.Sort;
import java.util.List;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    ProjectDTO findOneById(@NotNull String userId, @NotNull String id);

    void clear();

    void clear(@NotNull String userId);

}
