package ru.t1.rleonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.repository.model.ISessionRepository;
import ru.t1.rleonov.tm.api.service.model.ISessionService;
import ru.t1.rleonov.tm.exception.entity.SessionNotFoundException;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.model.Session;
import ru.t1.rleonov.tm.repository.model.SessionRepository;

@Service
@NoArgsConstructor
public final class SessionService extends AbstractService<Session, ISessionRepository>
        implements ISessionService {

    @NotNull
    @Autowired
    public SessionRepository repository;

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public Session create(@Nullable final Session session) {
        if (session == null) throw new SessionNotFoundException();
        repository.add(session);
        return session;
    }

    @Override
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public Session removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session = findOneById(id);
        if (session == null) throw new SessionNotFoundException();
        repository.remove(session);
        return session;
    }

}
