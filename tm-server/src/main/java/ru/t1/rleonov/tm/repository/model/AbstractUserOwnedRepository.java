package ru.t1.rleonov.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;

@NoArgsConstructor
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @NotNull
    protected String getSortType(@Nullable final Sort sort) {
        if (Sort.BY_CREATED == sort) return "created";
        if (Sort.BY_STATUS == sort) return "status";
        else return "name";
    }

}
