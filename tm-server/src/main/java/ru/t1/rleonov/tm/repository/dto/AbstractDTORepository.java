package ru.t1.rleonov.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.dto.IDTORepository;
import ru.t1.rleonov.tm.dto.model.AbstractModelDTO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IDTORepository<M> {

    @Getter
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        models.forEach(this::add);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
