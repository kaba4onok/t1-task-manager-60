package ru.t1.rleonov.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.model.IRepository;
import ru.t1.rleonov.tm.model.AbstractModel;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @Getter
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        models.forEach(this::add);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
