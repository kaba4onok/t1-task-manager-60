package ru.t1.rleonov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.rleonov.tm.dto.model.SessionDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDTORepository extends AbstractDTORepository<SessionDTO> implements ISessionDTORepository {

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String id) {
        if (id.isEmpty()) return null;
        return entityManager.find(SessionDTO.class, id);
    }

}
