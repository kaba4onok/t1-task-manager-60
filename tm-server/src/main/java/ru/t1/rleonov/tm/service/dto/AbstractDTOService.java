package ru.t1.rleonov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.repository.dto.IDTORepository;
import ru.t1.rleonov.tm.api.service.dto.IDTOService;
import ru.t1.rleonov.tm.dto.model.AbstractModelDTO;
import javax.persistence.EntityManager;
import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>>
        implements IDTOService<M> {

    @NotNull
    @Autowired
    protected IDTORepository<M> repository;

    @NotNull
    public EntityManager getEntityManager() {
        return repository.getEntityManager();
    }

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        repository.add(model);
    }

    @Override
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        repository.set(models);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        repository.update(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        repository.remove(model);
    }

}
