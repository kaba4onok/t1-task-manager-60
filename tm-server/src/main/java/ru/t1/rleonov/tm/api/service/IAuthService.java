package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.SessionDTO;
import ru.t1.rleonov.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable String login,
                     @Nullable String password,
                     @Nullable String email);

    @NotNull
    String login(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    void logout(@Nullable final SessionDTO session);

}
