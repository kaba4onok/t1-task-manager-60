package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.dto.model.UserDTO;

public final class UserTestData {

    @NotNull
    public final static UserDTO USER1 = new UserDTO();

    @NotNull
    public final static UserDTO USER2 = new UserDTO();

    @NotNull
    public final static UserDTO NEW_USER = new UserDTO();

    @NotNull
    public final static UserDTO ADMIN = new UserDTO();

    static {
        USER1.setLogin("user1");
        USER1.setPassword("user1");

        USER2.setLogin("user2");
        USER2.setPassword("user2");

        NEW_USER.setLogin("NEW_USER");
        NEW_USER.setPassword("NEW_USER");
        NEW_USER.setEmail("NEW_USER@EMAIL.RU");

        ADMIN.setLogin("admin");
        ADMIN.setPassword("admin");
        ADMIN.setRole(Role.ADMIN);
    }

}
