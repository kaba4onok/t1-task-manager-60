package ru.t1.rleonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ServerLoadDataYamlFasterXmlRequest;
import ru.t1.rleonov.tm.event.ConsoleEvent;

@Component
public final class DataYamlLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-load-yaml";

    @NotNull
    private static final String DESCRIPTION = "Load data from yaml file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlLoadFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ServerLoadDataYamlFasterXmlRequest request = new ServerLoadDataYamlFasterXmlRequest(getToken());
        getDomainEndpoint().loadDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
